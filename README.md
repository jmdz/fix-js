# Mis arreglos para JavaScript.

A lo largo de los años he juntado un montón de cosas que uso siempre para "arreglar" Javascript y este repositorio es donde medio les voy a dar forma de biblioteca.

## observeAttribute

O sea, un evento que se dispara cuando cambia el valor un atributo.

### Métodos

* `Node.prototype.observeAttribute($attributeName,$callback=function($mutationRecord,$node/*=this*/){})`
	* Crea un MutationObserver sobre el nodo actual para el atributo indicado.
* `MutationObserver.prototype.off()`
	* Método simple para desactivar un MutationObserver, ver `Node.prototype.observeAttribute`.


## Object

### Métodos

* `Object.chain($object,$methods)`
	* Ejecuta los metodos con los argumentos indicados en `$methods` sobre el objeto `$object` y devuelve el objeto `$object`.
	* `$methods` es un objeto plano donde cada propiedad es un metodo a llamar y el valor de estas son arrays de los argumentos a enviar a ese metodo (si no son array se convierten).
	* Ejemplo:
		* `let mydate = new Date(); Object.chain(mydate, {setDate:[1], setMonth:0});`
		* el valor devuelt por `Object.chain(...)` seria la variable `mydate` pero con el mes seteado a enero (`0`) y el dia a `1`.

## Array, ArrayBuffer, DataView y primos

### Métodos

* `Array.prototype.unique($strictEquality=true)`
	* Devuelve un nuevo array con los elementos únicos en el orden de su primer aparición.
* `Array.prototype.transpose()`
	* Devuelve un nuevo array con los elementos transpuestos.
* `Uint8Array.prototype.toHexArray($withPrefix=false)`
	* Convierte un Uint8Array en un Array sin tipo donde cada elemento es el correspondiente convertido a base hexadecimal. `$withPrefix` es si cada elemento debe comenzar con `0x` o no.
* `Uint8Array.prototype.toUint16Array($littleEndian=false)`
	* Convierte un Uint8Array en Uint16Array.
	* La endianidad es como se ordenan los bytes en un numero multibyte: 305419896 = 0x12345678
		* little-endian: 0x78 0x56 0x34 0x12
		* big-endian:    0x12 0x34 0x56 0x78
* `ArrayBuffer.prototype.getAsciiString($offset,$length=1)`
	* Convierte una parte de un buffer en un string considerando cada byte como un codepoint ASCII.
* `DataView.prototype.getAsciiString($offset,$length=1)`
	* Igual que `ArrayBuffer.prototype.getAsciiString`.
* `Uint16Array.prototype.toStringAsCharCode()`
	* Convierte un Uint16Array en un string considerando cada elemento como un codepoint Unicode.
* `Uint8Array.prototype.toStringAsCharCode()`
	* Ídem `Uint16Array.prototype.toStringAsCharCode`.

## Canvas

### Métodos

* `CanvasRenderingContext2D.prototype.fillEllipse($x,$y,$radiusX,$radiusY,$rotation,$startAngle,$endAngle,$anticlockwise=false)`
	* Como `fillRect` pero para `ellipse`.
* `CanvasRenderingContext2D.prototype.strokeEllipse($x,$y,$radiusX,$radiusY,$rotation,$startAngle,$endAngle,$anticlockwise=false)`
	+ Como `strokeRect` pero para `ellipse`.

## Date

### Propiedades

* `Date.timeZones`
	* Los husos horarios por su abreviatura militar.

### Métodos

* `Date.create(...)`
	* Devuelve un nuevo objeto Date creado con los argumentos recibidos.
* `Date.today()`
	* Milisegundos desde Epoch hasta el día en curso a las 0:00:00.000 en la zona horaria local. Similar a `Date.now()`.
* `Date.w($value=1)`
	* Milisegundos por semana.
* `Date.d($value=1)`
	* Milisegundos por día.
* `Date.h($value=1)`
	* Milisegundos por hora.
* `Date.m($value=1)`
	* Milisegundos por minuto.
* `Date.s($value=1)`
	* Milisegundos por segundo (solo por consistencia con los que anteriores).
* `Date.prototype.addMilliseconds($value,$returnThis=true)`
	* Suma una cantidad de milisegundos al tiempo actual del objeto. Si el parametro `returnThis` es `true` devuelve la instancia actual permitiendo el encadenado, si no devuelve el nuevo tiempo en milisegundos.
* `Date.prototype.getFullDate()`
	* Devuelve la fecha completa en formato `CCYY-MM-DD`. Como `toISOString` pero solo la fecha y sin conversión a UTC.
* `Date.prototype.getFullTime($milliseconds=false)`
	* Devuelve la hora completa en formato `HH:MM:SS[.FFF]`. Como `toISOString` pero solo la hora y sin conversión a UTC.
* `Date.prototype.getFullDateTime($separator=' ',$milliseconds=false)`
	+ Devuelve la fecha y hora completa en formato `CCYY-MM-DD(S)HH:MM:SS[.FFF]`. Como `toISOString` pero sin la obligación de usar `T` como separador y sin conversión a UTC.
* `Date.prototype.setFullDate($year=1970,$month=1,$day=1,$monthIsIndex=false,$returnThis=false)`
	* Establece el año, mes y fecha. Si `$monthIsIndex` es `true` enero es `0`, si es `false` enero es `1`.
	* Si alguno de los argumentos es `null` no asigna esa propiedad.
	* Si `$returnThis` es `false` devuelve los milisegundos desde Epoch después de hacer el cambio, caso contrario devuelve el propio objeto.
* `Date.prototype.setFullTime($hours=0,$minutes=0,$seconds=0,$milliseconds=0,$returnThis=false)`
	* Establece la hora, minutos, segundos y milisegundos.
	* Si alguno de los argumentos es `null` no asigna esa propiedad.
	* Si `$returnThis` es `false` devuelve los milisegundos desde Epoch después de hacer el cambio, caso contrario devuelve el propio objeto.
* `Date.prototype.getTimezoneValue($separator=':',$hour_padding=true)`
	* Devuelve el zona horaria en la forma `±[h]h:mm`. Por ejemplo para un valor de `getTimezoneOffset()` de `180`, `getTimezoneValue()` devolvería `'-03:00'`.
* `Date.prototype.getTimezones()`
	* Devuelve un objeto plano con las propiedades de `Date.timeZones` cuyos valores coinciden con `this.getTimezoneValue()`, agregando una propiedad `_` que asume el valor `false` o el nombre de la primera propiedad con coincidencia.

## File

### Métodos

* `File.prototype.getLastModifiedAsDate()`
	* Devuelve la fecha de la última modificación del archivo como objeto `Date`.
* `File.prototype.getLastModifiedAsISODateTime($separator=' ',$milliseconds=false)`
	* Devuelve la fecha de la última modificación del archivo como string llamando a `Date.prototype.getFullDateTime()`.
* `File.prototype.getSizeByteFormat($digits=2,$fixed=false,$si=false,$post_spaces=true,$pre_spaces=false)`
	* Devuelve el tamaño del archivo formateado con `Number.prototype.toByteFormat`.

## FormData

### Métodos

* `FormData.create($entries={/*name:value,...*/})`
	* Crea una instancia de FormData y añade las entradas que deben ser un objeto plano. Especialmente útil para los uploaders AJAX.
* `FormData.prototype.export()`
	* Simplemente una forma cómoda de ver los datos adentro de un FormData.

## Image

### Propiedades

* `HTMLImageElement.types`
	* Es un array con los tipos de imagen soportados por los navegadores.
	* Tiene una propiedad `aliases` que permite acceder a un tipo conociendo su exención o tipo MIME.
	* Cada tipo tiene los tipos MIME (el primero es el primario), las extensiones (la primera es la preferida) y los números mágicos.
	* Los números mágicos son un array de opciones igualmente validas donde cada opción es un array de condiciones todas necesarias (cada una con su posición de inicio, fin y valor en hexadecimal).
	* Funciona junto con el método `HTMLImageElement.prototype.loadType`.
* `HTMLImageElement.prototype.fromFile`
	* Ver `HTMLImageElement.prototype.loadFromFile`.
* `HTMLImageElement.prototype.blob`
	* Ver `HTMLImageElement.prototype.loadBlob`.
* `HTMLImageElement.prototype.type`
	* Ver `HTMLImageElement.prototype.loadType`.
* `HTMLImageElement.prototype.metadata`
	* Ver `HTMLImageElement.prototype.loadMetadata`.

### Métodos

* `HTMLImageElement.prototype.loadFromFile($file,$callback=function($file,$img/*=this*/){})`
	* Permite cargar una imagen desde un `input[type=file]` rápidamente.
	* Almacena en la propiedad de instancia `fromFile` datos relativos al archivo cargado.
* `HTMLImageElement.prototype.loadBlob($callback=function($blob,$isCache,$img/*=this*/){},$useCache=true)`
	* Permite cargar como binario una imagen en la propiedad `blob` de esta imagen.
* `HTMLImageElement.prototype.loadType($callback=function($type,$isCache,$img/*=this*/){},$useCache=true)`
	* Permite determinar el tipo de una imagen comparándola binariamente con los números mágicos de los tipos conocidos.
	* Almacena el tipo en la propiedad de instancia `type`.
* `HTMLImageElement.prototype.loadMetadata($callback=function($metadata,$isCache,$img/*=this*/){},$useCache=true)`
	* Carga los metadatos de una imagen en su propiedad `metadata`.
	* Para todas las imágenes crea un objeto `file` (en la propiedad `metadata`) que contiene el nombre, tamaño, tipo, ultima fecha de modificación, ancho natural y alto natural si están disponibles y luego ejecuta una función de carga de metadatos especifica para el tipo de imagen (estas están en bibliotecas separadas).
* `HTMLImageElement.prototype.reset($src=false,$styleTransform=false)`
	* Reinicia las propiedades creadas por esta biblioteca: `fromFile`, `blob`, etc.
* `HTMLImageElement.prototype.rotate($angle=0)`
	* Rota una imagen una cierta cantidad de grados (sexagesimales antihorarios, tal como es en matemática).
* `HTMLImageElement.prototype.flip($horizontally=false,$vertically=false)`
	+ Da vuelta una imagen de forma horizontal y/o vertical.
* `HTMLImageElement.prototype.flipH($c=true)`
	+ Da vuelta una imagen de forma horizontal. El parámetro permite poner el condicional dentro de la función y no por fuera.
* `HTMLImageElement.prototype.flipV($c=true)`
	+ Da vuelta una imagen de forma vertical. El parámetro permite poner el condicional dentro de la función y no por fuera.
* `HTMLImageElement.prototype.resizeBy($horizontally=1,$vertically=0)`
	* Re-dimensiona horizontal y verticalmente una imagen según las proporciones indicadas.
	* Si `$horizontally` es `0`, se la toma como `1` para evitar errores.
	* Si `$vertically` es `0`, se la toma como `$horizontally` para evitar errores. Esto también permite que si se desea ajustar una imagen manteniendo la relación de aspecto solo hace falta usar el primer parámetro.
	* `$horizontally` y `$vertically` se consideran siempre sin signo.
* `HTMLImageElement.prototype.resizeTo($width,$height)`
	* Redimensiona una imagen a un tamaño especifico en pixels.

## Math y Number (y algo de Array)

### Propiedades

* `Number.prototype.isFractional`
	* Booleana, por defecto `false`. Ver `Number.createFractional`.

### Métodos

* `Array.prototype.sum()`
	* Suma los elementos numéricos de un array y devuelve el resultado. Si el array no tiene ítems o no son numéricos devuelve `0`.

* `Array.prototype.avg()`
	* Devuelve el cociente entre la suma de los elementos numéricos con la longitud del array, si el array no tiene elementos devuelve `NaN`.

* `Array.prototype.min()`
	* Aplica `Math.min()` a los elementos del array.

* `Array.prototype.max()`
	* Aplica `Math.max()` a los elementos del array.

* `Math.roundTo($number,$decimals=0,$fixed=false)`
	* Similar a `Math.round()` pero con cantidad definida de decimales. si se establece `$fixed=true`, después de hacer el redondeo invoca a `Number.prototype.toFixed`.

* `Number.createFractional($numerator,$denominator)`
	* Crea una instancia de Number con el valor `$numerator/$denominator` y estableciendo tres propiedades: `numerator`, `denominator` y `isFractional=true`.

* `Number.prototype.toByteFormat($digits=2,$fixed=false,$si=false,$post_spaces=true,$pre_spaces=false)`
	* Formatea un numero como si fueran bytes para mostrarlo con la mayor unidad posible.
	* `$digits` es la cantidad de dígitos después del punto decimal.
	* `$fixed` es si se debe forzar a usar ceros para completar la cantidad de dígitos.
	* `$si` es si se debe usar el sistema internacional (base 10), por defecto usa el sistema del IEC (base 2).
	* `$post_spaces` espacios después de la abreviatura (para B).
	* `$pre_spaces` espacios después de la abreviatura (para B).

* `Number.prototype.toTimeFormat($digits=1,$fixed=false)`
	* Formatea un numero como si fueran milisegundos para mostrarlo con la mayor unidad posible, estas son: ms (milisegundo), s (segundo), m (minuto), h (hora), d (día), w (semana).
	* `$digits` es la cantidad de dígitos después del punto decimal.
	* `$fixed` es si se debe forzar a usar ceros para completar la cantidad de dígitos.

* `Number.prototype.toISOTimeFormat($milliseconds=false)`
	* Formatea un numero como una duración ISO 8601: `P{n}W{n}DT{n}H{n}M{n}[.{n}]S`.

## String

### Métodos

* `String.prototype.standardizeLineBreaks()`
	* Remplaza todos los saltos de linea por LF (0x0A).
* `String.prototype.template($variables={},$start='[#',$end='#]')`
	* Ejecuta el remplazo en el string dado de todos los elementos que empiecen por `$start` y terminen por `$end` con el valor de la propiedad o método en variables.
	* Ejemplo: `'esto es un [#uno#], y esto es otro [#uno] y esto es un [#dos#]'.template({uno:1,dos:function(){return 2;}})`.
	* Permite almacenar plantillas que no se evalúan hasta que se llama al método.
* `String.prototype.replace($s,$r)`
	* Es una sobrecarga del método `replace` estándar para que acepte arrays como parámetros o incluso un objeto plano.
	* Ejemplo: `'texto'.replace(['a','b'],[0,1])`.
	* Ejemplo: `'texto'.replace({a:0,b:1})`.
	* Cada elemento de los arrays es como los parámetros estándares.
* `String.prototype.basic_replace`
	* Es el `replace` estándar.
* `String.prototype.htmlspecialchars`
	* Remplaza todos las apariciones de los caracteres `&`, `"`, `'`, `<` y `>` por sus entidades: `&amp;`, `&quot;`, `&apos;`, `&lt;` y `&gt;`.

## URL

### Métodos

* `URL.prototype.getFileName()`
	* Devuelve el nombre del archivo de una URL. Nótese que puede ser el directorio final si la URL no incluye un nombre de archivo.

## *ListOne*

Un *ListOne* (perdón, era muy joven) es un *invento* de antes que se inventara JSON, es un array de strings almacenado en un string, donde el primer carácter es el separador: `'|hola|mundo' = ['hola','mundo']`.

Entonces un string vacío `''` es un array vacío `[]` y un string de un solo carácter `','` es un array con un solo elemento que es una cadena vacía `['']`.

### Métodos

* `Array.fromListOne($listOne='')`
	* Convierte un string *ListOne* en un array.
* `Array.prototype.toListOne($charSeparator)`
	* Convierte un array en un string *ListOne*. `$charSeparator` debe ser un solo carácter y no debe encontrarse en los elementos del array que deben ser strings.
* `Object.fromListOne($listOne='',$assignOperator=null)`
	* Es una extensión de los arrays *ListOne* suponiendo que cada elemento es una propiedad.
	* `$assignOperator` es el carácter que separa el nombre de la propiedad del valor de la misma en cada elemento del array, si es nulo es el primer carácter que no es un numero, letra, guion medio, guion de subrayado o espacio (si, los espacios son validos en los nombres de propiedades de estos objetos).
	* Ejemplos:
		* `Object.fromListOne(';uno,1;dos,Dos',',') = {'uno':'1','dos':'Dos'}`
		* `Object.fromListOne('&uno=1&dos=Dos') = {'uno':'1','dos':'Dos'}`

## Log

* `window.log($var,$ref='',$showType=false)`
	* Solo un log realmente rápido.

## Ejemplos en vivo.

Todos mis experimentos (que para eso están): [jmdz.com.ar/apps](http://jmdz.com.ar/apps) y [gitlab/jmdz/my-web](https://gitlab.com/jmdz/my-web).
