window.isDefined=function($var){
	return typeof($var)!=='undefined';
};

window.log=function($var,$ref='',$showType=false){
	if($ref==''){
		if($showType){
			console.log('('+typeof($var)+')',$var);
		}
		else{
			console.log($var);
		}
	}
	else{
		if($showType){
			console.log($ref,'=','('+typeof($var)+')',$var);
		}
		else{
			console.log($ref,'=',$var);
		}
	}
};

Object.chain = function($obj, $methods) {
	for (let [method, args] of Object.entries($methods)) {
		if (typeof $obj[method] === "function") {
			if (!Array.isArray(args)) args = [args];
			$obj[method](...args);
		}
	}
	return $obj;
}

Array.fromListOne=function($listOne=''){
	return $listOne.slice(1).split($listOne.slice(0,1));
};

Array.prototype.unique=function($strictEquality=true){
	let f=$strictEquality
		?function($v,$i,$a){return $a.findIndex(function($e){return $e===$v;})===$i;}
		:function($v,$i,$a){return $a.findIndex(function($e){return $e==$v;})===$i;}
	;
	return this.filter(f);
};

Array.prototype.transpose = function() {
	let r = [];
	for (let i = 0; i < this.length; i++) {
		for (let j = 0; j < this[i].length; j++) {
			if (typeof r[j] === 'undefined') {
				r[j] = [];
			}
			r[j][i] = this[i][j];
		}
	}
	return r;
};

Array.prototype.toListOne=function($charSeparator){
	if(this.length){
		return ''+$charSeparator+this.join($charSeparator);
	}
	else{
		return '';
	}
};

Array.prototype.sum=function(){
	return this.reduce(($a,$v)=>isNaN(+$v)?0:$a+$v,0);
};

Array.prototype.avg=function(){
	return this.sum()/this.length;
};

Array.prototype.min=function(){
	return Math.min.apply(Math,this);
};

Array.prototype.max=function(){
	return Math.max.apply(Math,this);
};

ArrayBuffer.prototype.getAsciiString=function($offset,$length=1){
	return (
		new Uint8Array(
			this
			,$offset
			,$length
		)
	).toStringAsCharCode();
};

CanvasRenderingContext2D.prototype.fillEllipse=function($x,$y,$radiusX,$radiusY,$rotation,$startAngle,$endAngle,$anticlockwise=false){
	this.beginPath();
	this.ellipse($x,$y,$radiusX,$radiusY,$rotation,$startAngle,$endAngle,$anticlockwise);
	this.fill();
};

CanvasRenderingContext2D.prototype.strokeEllipse=function($x,$y,$radiusX,$radiusY,$rotation,$startAngle,$endAngle,$anticlockwise=false){
	this.beginPath();
	this.ellipse($x,$y,$radiusX,$radiusY,$rotation,$startAngle,$endAngle,$anticlockwise);
	this.stroke();
};

DataView.prototype.getAsciiString=function($offset,$length=1){
	return this.buffer.getAsciiString($offset,$length);
};

Date.timeZones={
	J:'/LOCAL',Z:'+00:00'
	,Y:'-12:00',X:'-11:00',W:'-10:00',V:'-09:00',U:'-08:00',T:'-07:00'
	,S:'-06:00',R:'-05:00',Q:'-04:00',P:'-03:00',O:'-02:00',N:'-01:00'
	,A:'+01:00',B:'+02:00',C:'+03:00',D:'+04:00',E:'+05:00',F:'+06:00'
	,G:'+07:00',H:'+08:00',I:'+09:00',K:'+10:00',L:'+11:00',M:'+12:00'
};

Date.s=function($value=1){return $value*1000;};

Date.m=function($value=1){return $value*Date.s(60);};

Date.h=function($value=1){return $value*Date.m(60);};

Date.d=function($value=1){return $value*Date.h(24);};

Date.w=function($value=1){return $value*Date.d(7);};

Date.today=function(){
	return (new Date()).setFullTime();
};

Date.create=function(){return new Date(...arguments);};

Date.prototype.addMilliseconds=function($value,$returnThis=true){
	this.setTime(this.getTime()+$value);
	return $returnThis?this:this.getTime();
};

Date.prototype.getFullDate=function(){
	return ''
		+this.getFullYear()
		+'-'+(this.getMonth()+1).toString().padStart(2,0)
		+'-'+this.getDate().toString().padStart(2,0)
	;
};

Date.prototype.getFullTime=function($milliseconds=false){
	return ''
		+this.getHours().toString().padStart(2,0)
		+':'+this.getMinutes().toString().padStart(2,0)
		+':'+this.getSeconds().toString().padStart(2,0)
		+(
			$milliseconds
			?('.'+this.getMilliseconds().toString().padStart(3,0))
			:''
		)
	;
};

Date.prototype.getFullDateTime=function($separator=' ',$milliseconds=false){
	return this.getFullDate()+$separator+this.getFullTime($milliseconds);
};

Date.prototype.setFullDate=function($year=1970,$month=1,$day=1,$monthIsIndex=false,$returnThis=false){
	if($year!==null) this.setFullYear($year+($year<70?2000:($year<100?1900:0)));
	if($month!==null) this.setMonth($month-($monthIsIndex?0:1));
	if($day!==null) this.setDate($day);
	return $returnThis?this:this.getTime();
};

Date.prototype.setFullTime=function($hours=0,$minutes=0,$seconds=0,$milliseconds=0,$returnThis=false){
	if($hours!==null) this.setHours($hours);
	if($minutes!==null) this.setMinutes($minutes);
	if($seconds!==null) this.setSeconds($seconds);
	if($milliseconds!==null) this.setMilliseconds($milliseconds);
	return $returnThis ? this : this.getTime();
};

Date.prototype.getTimezoneValue=function($separator=':',$hour_padding=true){
	let o=this.getTimezoneOffset();
	const s=o<0?'+':'-';
	o=Math.abs(o);
	const m=o%60;
	const h=(o-m)/60;
	return ''+s+($hour_padding&&h<10?'0':'')+h+$separator+(m<10?'0':'')+m;
};

Date.prototype.getTimezones=function(){
	let r={_:false};
	const t=this.getTimezoneValue();
	for (const k of Object.getOwnPropertyNames(Date.timeZones)) {
		if(Date.timeZones[k]===t){
			if(!r._)r._=k;
			r[k]=Date.timeZones[k];
		}
	}
	return r;
};

FormData.create=function($entries={/*name:value,...*/}){
	var obj=new FormData();
	Object.entries($entries).forEach(
		function($v){
			if($v[1] instanceof File){
				obj.append($v[0],$v[1],$v[1].name);
			}
			else{
				obj.append($v[0],$v[1]);
			}
		}
	);
	return obj;
};

FormData.prototype.export=function(){
	var obj={};
	for(var pair of this.entries()){
		obj[pair[0]]=pair[1];
	}
	return obj;
};

HTMLImageElement.prototype.fromFile=null;

HTMLImageElement.prototype.loadFromFile=function(
	$file
	,$callback=function($file,$img/*=this*/){}
){
	if(
		!($file instanceof File)
		&& $file instanceof HTMLInputElement
		&& $file.type.toLowerCase()==='file'
		&& $file.files
		&& $file.files[0]
		&& $file.files[0] instanceof File
	){
		$file=$file.files[0];
	}
	if($file instanceof File){
		let that=this;
		let reader=new FileReader();
		reader.onload=function($e){
			that.src=$e.target.result;
			that.fromFile=$file;
			$callback.apply(that,[that.fromFile,that]);
		};
		reader.readAsDataURL($file);
		return true;
	}
	else{
		return false;
	}
};

HTMLImageElement.prototype.blob=null;

HTMLImageElement.prototype.loadBlob=function(
	$callback=function($blob,$isCache,$img/*=this*/){}
	,$useCache=true
){
	let that=this;
	if($useCache&&that.blob&&that.blob.source&&that.blob.source==that.src){
		$callback.apply(that,[that.blob,true,that]);
	}
	else{
		that.blob=null;
		fetch(that.src)
			.then(function($r){
				return $r.blob();
			})
			.then(function($blob){
				that.blob=$blob;
				that.blob.source=that.src;
				$callback.apply(that,[that.blob,false,that]);
			})
		;
	}
};

{HTMLImageElement.types=[
	// mimes: primary, others/aliases
	// extensions: preferred, others
	// magic: is an array of or-conditions, each or-condition is an array of and-conditions
	{ mimes:['image/webp'                       ],extensions:['webp'                                     ],magic:[ [{begin:0,end:4,value:'52494646'}                 ,{begin:8,end:12,value:'57454250'}]                                          ]}
	,{mimes:['image/jpeg','image/pjpeg'         ],extensions:['jpg','jpeg','jpe','jif','jfif','jfi'      ],magic:[ [{begin:0,end:3,value:'ffd8ff'}                                                     ]                                          ]}
	,{mimes:['image/png'                        ],extensions:['png'                                      ],magic:[ [{begin:0,end:8,value:'89504e470d0a1a0a'}                                           ]                                          ]}
	,{mimes:['image/gif'                        ],extensions:['gif'                                      ],magic:[ [{begin:0,end:6,value:'474946383761'}                                               ] , [{begin:0,end:6,value:'474946383961'}] ]}
	,{mimes:['image/x-icon'                     ],extensions:['ico'                                      ],magic:[ [{begin:0,end:4,value:'00000100'}                                                   ]                                          ]}
	,{mimes:['image/bmp','image/x-bmp'          ],extensions:['bmp','dib'                                ],magic:[ [{begin:0,end:2,value:'424d'}                                                       ]                                          ]}
	,{mimes:['image/tiff','image/tiff-fx'       ],extensions:['tif','tiff'                               ],magic:[ [{begin:0,end:4,value:'49492a00'}                                                   ] , [{begin:0,end:4,value:'4d4d002a'}    ] ]}
	,{mimes:['image/jp2','image/jpx','image/jpm'],extensions:['jp2','jpg2','jpf','jpx','jpm','jpgm','j2k'],magic:[ [{begin:0,end:12,value:'0000000c6a5020200d0a870a'}                                  ]                                          ]}
	,{mimes:['image/jxr'                        ],extensions:['jxr'                                      ],magic:[ [{begin:0,end:3,value:'4949bc'}                                                     ]                                          ]}
	,{mimes:['image/x-mng'                      ],extensions:['mng'                                      ],magic:[ [{begin:0,end:0,value:'8a4d4e470d0a1a0a'}                                           ]                                          ]}
];(function(){
	let t=HTMLImageElement.types;
	t.aliases={};
	for(let i=0;i<t.length;i++){
		for(let e=0;e<t[i].extensions.length;e++){
			t.aliases[
				t[i].extensions[e]
			]=t[i];
		}
		for(let m=0;m<t[i].mimes.length;m++){
			t.aliases[
				t[i].mimes[m]
			]=t[i];
		}
	}
})();}

HTMLImageElement.prototype.type=null;

HTMLImageElement.prototype.loadType=function(
	$callback=function($type,$isCache,$img/*=this*/){}
	,$useCache=true
){
	let that=this;
	if($useCache&&that.type&&that.blob&&that.blob.source&&that.blob.source==that.src){
		$callback.apply(that,[that.type,true,that]);
	}
	else{
		that.type=null;
		that.loadBlob(async function(){
			this.type=this.blob.type;
			if(!this.type){
				let ab=await (new Response(this.blob)).arrayBuffer();
				for(let t=0;t<HTMLImageElement.types.length;t++){
					for(let m=0;m<HTMLImageElement.types[t].magic.length;m++){
						let b=0;
						for(let c=0;c<HTMLImageElement.types[t].magic[m].length;c++){
							let v=Array.from(
								new Uint8Array(
									ab.slice(
										HTMLImageElement.types[t].magic[m][c].begin
										,HTMLImageElement.types[t].magic[m][c].end
									)
								)
							)
							.map(function($v){
								return $v.toString(16).padStart(2,'0');
							}).join('');
							if(v===HTMLImageElement.types[t].magic[m][c].value){
								b++;
							}
						}
						if(b===HTMLImageElement.types[t].magic[m].length){
							this.type=HTMLImageElement.types[t].mimes[0];
						}
						if(this.type){
							break;
						}
					}
					if(this.type){
						break;
					}
				}
			}
			$callback.apply(this,[this.type,false,this]);
		},$useCache);
	}
};

HTMLImageElement.prototype.metadata=null;

HTMLImageElement.prototype.loadMetadata=function(
	$callback=function($metadata,$isCache,$img/*=this*/){}
	,$useCache=true
){
	let that=this;
	if($useCache&&that.metadata&&that.blob&&that.blob.source&&that.blob.source==that.src){
		$callback.apply(that,[that.metadata,true,that]);
	}
	else{
		that.metadata=null;
		that.loadType(function(){
			if(
				HTMLImageElement.types.aliases[this.type]
				&&
				HTMLImageElement.types.aliases[this.type].loadMetadata
			){
				that.metadata={
					file:{
						type:that.type
						,size:that.blob.size
						,name:(
							that.fromFile
							?that.fromFile.name
							:(new URL(that.src)).getFileName()
						)
						,date:(
							that.fromFile
							?(new Date(that.fromFile.lastModified))
							:null
						)
						,y:that.naturalHeight
						,x:that.naturalWidth
					}
					,exif:null
					,iptc:null
					,xmp:null
					,fits:null
				};
				HTMLImageElement.types.aliases[this.type].loadMetadata(this,$callback);
			}
			else{
				$callback.apply(this,[this.metadata,false,this]);
			}
		},$useCache);
	}
};

HTMLImageElement.prototype.reset=function($src=false){
	this.fromFile=null;
	this.blob=null;
	this.type=null;
	this.metadata=null;
	if($src){
		this.src='';
	}
};

HTMLImageElement.prototype.rotate=function($angle=0){
	try{
		let canvas,context,r,x,y;
		canvas=document.createElement('canvas');
		context=canvas.getContext('2d');
		r=$angle/-180*Math.PI;
		canvas.width=Math.round(
			this.naturalHeight*Math.abs(Math.sin(r))
			+this.naturalWidth*Math.abs(Math.cos(r))
		);
		canvas.height=Math.round(
			this.naturalWidth*Math.abs(Math.sin(r))
			+this.naturalHeight*Math.abs(Math.cos(r))
		);
		x=Math.round(canvas.width/2);
		y=Math.round(canvas.height/2);
		context.translate(x,y);
		context.rotate(r);
		context.translate(-x,-y);
		context.drawImage(
			this
			,x-Math.round(this.naturalWidth/2)
			,y-Math.round(this.naturalHeight/2)
			,this.naturalWidth
			,this.naturalHeight
		);
		this.src=canvas.toDataURL();
	}
	catch(e){
		console.log('Error rotate',this,$angle,e);
	}
	return this;
};

HTMLImageElement.prototype.flip=function($horizontally=false,$vertically=false){
	try{
		let canvas,context;
		canvas=document.createElement('canvas');
		context=canvas.getContext('2d');
		canvas.width=this.naturalWidth;
		canvas.height=this.naturalHeight;
		context.translate(
			$horizontally?this.naturalWidth:0
			,$vertically?this.naturalHeight:0
		);
		context.scale(
			$horizontally?-1:1
			,$vertically?-1:1
		);
		context.drawImage(this,0,0);
		this.src=canvas.toDataURL();
	}
	catch(e){
		console.log('Error flip',this,$horizontally,$vertically,e);
	}
	return this;
};

HTMLImageElement.prototype.flipH=function($c=true){
	return this.flip($c,false);
};

HTMLImageElement.prototype.flipV=function($c=true){
	return this.flip(false,$c);
};

HTMLImageElement.prototype.resizeBy=function($horizontally=1,$vertically=0){
	if(!$horizontally)$horizontally=1;
	if(!$vertically)$vertically=$horizontally;
	$horizontally=Math.abs($horizontally);
	$vertically=Math.abs($vertically);
	try{
		let canvas,context;
		canvas=document.createElement('canvas');
		context=canvas.getContext('2d');
		canvas.width=Math.round(this.naturalWidth*$horizontally);
		canvas.height=Math.round(this.naturalHeight*$vertically);
		context.drawImage(this,0,0,canvas.width,canvas.height);
		this.src=canvas.toDataURL();
	}
	catch(e){
		console.log('Error resize',this,$horizontally,$vertically,e);
	}
	return this;
};

HTMLImageElement.prototype.resizeTo=function($width,$height){
	return this.resizeBy($width/this.naturalWidth,$height/this.naturalHeight);
};

Math.roundTo=function($number,$decimals=0,$fixed=false){
	var factor=Math.pow(10,parseInt($decimals));
	$number=Math.round(parseFloat($number)*factor)/factor;
	return $fixed?$number.toFixed($decimals):$number;
};

MutationObserver.prototype.off=function(){
	this.disconnect();
};

Node.prototype.observeAttribute=function(
	$attributeName
	,$callback=function($mutationRecord,$node/*=this*/){}
){
	let observer=new MutationObserver(function($list){
		$list.forEach(function($record){
			if($record.attributeName===$attributeName){
				$callback.apply($record.target,[$record,$record.target]);
			}
		});
	});
	observer.observe(this,{attributes:true});
	return observer;
};

File.prototype.getLastModifiedAsDate=function(){
	return new Date(this.lastModified);
};

File.prototype.getLastModifiedAsISODateTime=function($separator=' ',$milliseconds=false){
	return this.getLastModifiedAsDate().getFullDateTime($separator,$milliseconds);
};

File.prototype.getSizeByteFormat=function($digits=2,$fixed=false,$si=false,$post_spaces=true,$pre_spaces=false){
	return this.size.toByteFormat($digits,$fixed,$si,$post_spaces,$pre_spaces);
};

Number.prototype.isFractional=false;

Number.createFractional=function($numerator,$denominator){
	let n=new Number($numerator/$denominator);
	n.isFractional=true;
	n.numerator=$numerator;
	n.denominator=$denominator;
	return n;
};

Number.prototype.toByteFormat=function($digits=2,$fixed=false,$si=false,$post_spaces=true,$pre_spaces=false){
	var bytes=Math.floor(Math.abs(this));
	if($si){
		var base=1000;
		var units=[
			' '.repeat(($digits+1)*($pre_spaces?1:0))+'B'+($post_spaces?' ':'')
			,'kB','MB','GB','TB','PB','EB','ZB','YB'
		];
	}
	else{
		var base=1024;
		var units=[
			' '.repeat(($digits+1)*($pre_spaces?1:0))+'B'+($post_spaces?'  ':'')
			,'KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'
		];
	}
	for(var i=0;base<bytes&&i<units.length;i++){
		bytes=bytes/base;
	}
	return Math.roundTo(bytes,$digits,$fixed)+' '+units[i];
};

Number.prototype.toISOTimeFormat=function($milliseconds=false){
	var o='P';
	var t=Math.floor(Math.abs(this));
	if(t>=Date.w()){
		o+=Math.floor(t/Date.w())+'W';
		t=t%Date.w();
	}
	if(t>=Date.d()){
		o+=Math.floor(t/Date.d())+'D';
		t=t%Date.d();
	}
	o+='T';
	if(t>=Date.h()){
		o+=Math.floor(t/Date.h())+'H';
		t=t%Date.h();
	}
	if(t>=Date.m()){
		o+=Math.floor(t/Date.m())+'M';
		t=t%Date.m();
	}
	if(t>=Date.s()){
		o+=Math.floor(t/Date.s());
		t=t%Date.s();
		if(t>0&&$milliseconds){
			o+='.'+t;
		}
		o+='S';
	}
	return o;
};

Number.prototype.toTimeFormat=function($digits=1,$fixed=false){
	var time=Math.floor(Math.abs(this));
	var bases=[1000,60,60,24,7];
	var units=['ms','s','m','h','d','w'];
	for(var i=0;bases[i]<time&&i<units.length;i++){
		time=time/bases[i];
	}
	return Math.roundTo(time,$digits,$fixed)+' '+units[i];
};

Object.fromListOne=function($listOne='',$assignOperator=null){
	if(
		$assignOperator!==null
		&&
		(typeof($assignOperator)!=='string'||$assignOperator.length!==1)
	){
		$assignOperator=null;
	}
	let o={};
	Array.fromListOne($listOne).forEach(function($v){
		let s;
		if($assignOperator){
			s=$assignOperator;
		}
		else{
			s=/[^a-zA-Z0-9_\-\s]/.exec($v);
			if(s){
				s=s[0];
			}
		}
		if(s){
			let i=$v.indexOf(s);
			if(i!=-1){
				let p=$v.slice(0,i).trim();
				let v=$v.slice(i+1).trim();
				if(p!=''){
					o[p]=v;
				}
			}
		}
	});
	return o;
};

String.prototype.standardizeLineBreaks=function(){
	return this.replace(/\r\n/g,'\n').replace(/\n\r/g,'\n').replace(/\r/g,'\n');
};

String.prototype.template=function($variables={},$start='[#',$end='#]'){
	let r='',o=0,s,v,e;
	do{
		s=this.indexOf($start,o);
		if(s>-1){
			e=this.indexOf($end,s);
			v=this.slice(s+$start.length,e);
			r+=this.slice(o,s)
				+(
					typeof($variables[v])==='function'
					?$variables[v]()
					:$variables[v]
				)
			;
			o=e+$end.length;
		}
		else{
			r+=this.slice(o);
		}
	}while(s>-1);
	return r;
};

String.prototype.basic_replace=String.prototype.replace;
String.prototype.replace=function($s,$r){
	if(($s instanceof Array)&&($r instanceof Array)&&$s.length===$r.length){
		let t=''+this;
		for(let i=0;i<$s.length;i++){
			t=t.basic_replace($s[i],$r[i]);
		}
		return t;
	}
	else if(($s instanceof Object)&&!isDefined($r)){
		let t=''+this;
		for(let p of Object.keys($s)){
			t=t.basic_replace(p,$s[p]);
		}
		return t;
	}
	else{
		return this.basic_replace($s,$r);
	}
};

String.prototype.htmlspecialchars=function(){
	return this.replace(
		[/&/g,   /"/g,    /'/g,    /</g,  />/g  ],
		['&amp;','&quot;','&apos;','&lt;','&gt;']
	);
};

Uint16Array.prototype.toStringAsCharCode=function(){
	return Array.from(
		this
		,function($v){return String.fromCharCode($v);}
	).join('');
};

Uint8Array.prototype.toHexArray=function($withPrefix=false){
	return Array.from(this,function($v){return ($withPrefix?'0x':'')+$v.toString(16).toUpperCase().padStart(2,0);})
};

Uint8Array.prototype.toStringAsCharCode=function(){
	return Array.from(
		this
		,function($v){return String.fromCharCode($v);}
	).join('');
};

Uint8Array.prototype.toUint16Array=function($littleEndian=false){
	if(this.length%2===0){
		return new Uint16Array(
			this.toHexArray(false)
				.map(
					$littleEndian
					?function($v,$i,$a){return $i%2?'0x'+$v+$a[$i-1]:0;}
					:function($v,$i,$a){return $i%2?'0x'+$a[$i-1]+$v:0;}
				)
				.filter(function($v){return $v!==0;})
				.map(function($v){return +$v;})
		);
	}
	else{
		return false;
	}
};

URL.prototype.getFileName=function(){
	switch(this.protocol){
		case 'ftp:':
		case 'http:':
		case 'https:':
			return this.pathname.slice(this.pathname.lastIndexOf('/')+1);
		default:
			return null;
	}
};
